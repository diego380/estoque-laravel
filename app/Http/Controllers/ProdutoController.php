<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Produto;
use Request;

/**
* 
*/
class ProdutoController extends Controller{
	
	function lista(){
		
		$produtos = DB::select('select * from produtos');
		
		return view('listagem')->with('produtos',$produtos);
	}

	function mostra(){
		$id = Request::route('id');
		$resposta = DB::select('select * from produtos where id= ?',[$id]);
		if(empty($resposta)) {
			return "<h1>Esse produto não existe</h1>";
		}
		return view('detalhes')->with('p', $resposta[0]);
	}
}



