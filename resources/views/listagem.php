<!DOCTYPE html>
<html>
<head>
	<title>Lista</title>
	<link rel="stylesheet" href="/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container">
		<h1>Listagem de produtos</h1>
		<table class="table table-striped table-bordered table-hover">
			<?php foreach ($produtos as $p): ?>
				<tr>
					<td><?=$p->nome?></td>
					<td><?=$p->valor?></td>
					<td><?=$p->descricao?></td>
					<td><?=$p->quantidade?></td>
					<td>
						<a href="produtos/mostra/<?=$p->id?>">
						<span class="glyphicon glyphicon-search"></span>
						</a>
					</td>
				</tr>
			<?php endforeach ?>
		</table>
	</div>
	

	
</body>
</html>