<!DOCTYPE html>
<html>
<head>
	<title>Detalhes</title>
	<link rel="stylesheet" href="/css/bootstrap.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container">
		<h1>Detalhes do produto: <?=$p->nome?></h1>
		<ul>
			<li>
				<b>Descrição:</b> <?=$p->descricao?>
			</li>
			<li>
				<b>Qtd:</b> <?=$p->quantidade?>
			</li>
			<li>
			<b>Valor:</b> R$ <?=$p->valor?>
			</li>
		</ul>
	</div>
	
</body>
</html>